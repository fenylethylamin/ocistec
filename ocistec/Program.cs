﻿using System;
using System.Collections.Generic;

namespace ocistec
{
    class Player
    {
        public string Name { get; set; }
        public int Karma { get; set; }
        public int positionX { get; set; }
        public int positionY { get; set; }
        public Player(int starting_Karma = 0)
        {
            Karma = starting_Karma;
            positionX = 0;
            positionY = 0;
        }
        public int GetKarma()
        {
            return Karma;
        }
        public void IncreaseKarma()
        {
            Karma++;

            Console.WriteLine("Získal jsi karmu. Tvoje aktuální karme je " + Karma + ".");
        }
        public void LoseKarma()
        {
            Karma--;
            Console.WriteLine("Přišel jsi o karmu. Tvoje aktuální karme je " + Karma + ".");
        }
        public Coord GetPosition()
        {
            return new Coord(positionX, positionY);
        }
    }
    class Coord
    {
        public int x, y;
        public Coord(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
    class Demon
    {
        public int demon_health { get; set; }
        public int demonPositionX { get; set; }
        public int demonPositionY { get; set; }
        private string _name;

        public Demon(int posX, int posY, string name)
        {
            _name = name;
            demonPositionX = posX;
            demonPositionY = posY;
        }

        public string GetName()
        {
            return _name;
        }
    }
    class Question
    {
        public Question(int posX, int posY, string questionText, string correctAnswer, List<string> answers)
        {
            question_positionx = posX;
            question_positiony = posY;
            CorrectAnswer = correctAnswer;
            _answers = answers;
            QuestionText = questionText;
        }
        public string QuestionText { get; set; }
        private List<string> _answers;
        public string CorrectAnswer { get; set; }
        public int question_positionx { get; set; }
        public int question_positiony { get; set; }

        public string GetQuestion()
        {
            return QuestionText;
        }
        public void ShowAnswers()
        {
            for (int i = 0; i < _answers.Count; i++)
            {
                Console.WriteLine(_answers[i]);
            }
        }


    }
    enum FieldType
    {
        START,
        END,
        DEMON,
        QUESTION
    }

    class Field
    {
        private Question _question;
        private Demon _demon;
        private FieldType _fieldType;
        private bool _visited = false;

        public void SetQuestion(Question q)
        {
            _question = q;
        }

        public void SetDemon(Demon q)
        {
            _demon = q;
        }

        public void SetFieldType(FieldType q)
        {
            _fieldType = q;
        }

        public Question GetQuestion()
        {
            return _question;
        }
        public Demon GetDemon()
        {
            return _demon;
        }

        public FieldType GetFieldType()
        {
            return _fieldType;
        }

        public void VisitField()
        {
            _visited = true;
        }

        public bool WasVisited()
        {
            return _visited;
        }
    }
    class Program
    {
        private static Field[,] _field;

        static int map_size_width = 3;
        static int map_size_height = 3;

        private static bool _isRunnning = false;

        static bool MoveLeft(Player player)
        {
            if (player.positionX - 1 < 0)
                return false;
            else
            {
                player.positionX = player.positionX - 1;
                return true;
            }
        }
        static bool MoveRight(Player player)
        {
            if (player.positionX + 1 > map_size_width - 1)
                return false;
            else
            {
                player.positionX = player.positionX + 1;
                return true;
            }
        }
        static bool MoveUp(Player player)
        {
            if (player.positionY - 1 < 0)
                return false;
            else
            {
                player.positionY = player.positionY - 1;
                return true;
            }
        }
        static bool MoveDown(Player player)
        {
            if (player.positionY + 1 > map_size_height - 1)
                return false;
            else
            {
                player.positionY = player.positionY + 1;
                return true;

            }
        }
        static void RenderPosition(Player player)
        {
            Console.WriteLine(player.positionX);
            Console.WriteLine(player.positionY);
        }

        static void PlayerMovement(Player player)
        {
            Console.WriteLine("Kudy se vydáš?");
            GetPosibleDirections(player);

            string direction = Console.ReadLine();
            // Console.WriteLine (direction);
            bool success = false;
            switch (direction)
            {
                case "doleva":
                    success = MoveLeft(player);
                    break;
                case "doprava":
                    success = MoveRight(player);
                    break;
                case "nahoru":
                    success = MoveUp(player);
                    break;
                case "dolu":
                    success = MoveDown(player);
                    break;
                default:
                    Console.WriteLine("Můžeš jít doleva, doprava, nahoru a dolu.");
                    PlayerMovement(player);
                    break;
            }

            if (!success)
            {
                Console.WriteLine(direction + " to nepůjde.");
                PlayerMovement(player);
            }
        }

        static void GetPosibleDirections(Player player)
        {
            string s = "Můžeš jít následujícími směry: ";
            if (player.GetPosition().x > 0)
            {
                s += " doleva";
            }

            if (player.GetPosition().x < map_size_width - 1)
            {
                s += " doprava";
            }

            if (player.GetPosition().y > 0)
            {
                s += " nahoru";
            }

            if (player.GetPosition().y < map_size_height - 1)
            {
                s += " dolu";
            }

            Console.WriteLine(s);
        }

        static void Fight(Player player)
        {
            Console.WriteLine("Jaký útok zvolíš? Můžeš použít kámen, nůžky nebo papír.");
            string attack = Console.ReadLine();
            // Console.WriteLine( attack );
            Random rd = new Random();

            int enemyAttack = rd.Next(0, 3);
            switch (enemyAttack)
            {
                case 0:
                    Console.WriteLine("Démon zaútočil kamenem.");
                    break;
                case 1:
                    Console.WriteLine("Démon zaútočil nůžkami..");
                    break;
                case 2:
                    Console.WriteLine("Démon zaútočil papírem.");
                    break;
            }
            switch (attack)
            {
                case "kamen":
                    if (enemyAttack == 0)
                    {
                        Console.WriteLine("Remíza! Zaútoč ještě jednou.");
                        Fight(player);
                    }
                    else if (enemyAttack == 1)
                    {
                        player.IncreaseKarma();
                    }
                    else
                    {
                        player.LoseKarma();
                    }
                    break;
                case "nuzky":
                    if (enemyAttack == 0)
                    {
                        player.LoseKarma();
                    }
                    else if (enemyAttack == 1)
                    {
                        Console.WriteLine("Remíza! Zaútoč ještě jednou.");
                        Fight(player);
                    }
                    else
                    {
                        player.IncreaseKarma();
                    }
                    break;
                case "papir":
                    if (enemyAttack == 0)
                    {
                        player.IncreaseKarma();
                    }
                    else if (enemyAttack == 1)
                    {
                        player.LoseKarma();
                    }
                    else
                    {
                        Console.WriteLine("Remíza! Zaútoč ještě jednou.");
                        Fight(player);
                    }
                    break;

                default:
                    Console.WriteLine("Neplatný útok.");
                    Fight(player);
                    break;
            }
        }

        static void Main(string[] args)
        {
            _field = new Field[map_size_width, map_size_height];
            for (int x = 0; x < map_size_width; x++)
            {
                for (int y = 0; y < map_size_height; y++)
                {
                    _field[x, y] = new Field();
                }
            }

            Player player = new Player();

            Demon demon1 = new Demon(2, 0, "tvou lenost.");

            _field[demon1.demonPositionX, demon1.demonPositionY].SetDemon(demon1);
            _field[demon1.demonPositionX, demon1.demonPositionY].SetFieldType(FieldType.DEMON);


            Demon demon2 = new Demon(0, 1, "tvou závist a nepřejícnost.");

            _field[demon2.demonPositionX, demon2.demonPositionY].SetDemon(demon2);
            _field[demon2.demonPositionX, demon2.demonPositionY].SetFieldType(FieldType.DEMON);

            Demon demon3 = new Demon(1, 1, "tvoje pozemské hýření.");
            _field[demon3.demonPositionX, demon3.demonPositionY].SetDemon(demon3);
            _field[demon3.demonPositionX, demon3.demonPositionY].SetFieldType(FieldType.DEMON);

            var questionA = new Question(1, 0, "Která z následujících osobností je tvému srdci bližší: Ježíš, nebo Hitler?", "Jezis", new List<string>()
         {
            "Jezis",
            "Hitler"

         });
            _field[questionA.question_positionx, questionA.question_positiony].SetQuestion(questionA);
            _field[questionA.question_positionx, questionA.question_positiony].SetFieldType(FieldType.QUESTION);

            var questionB = new Question(0, 2, "Jaká vlastnost tě nejlépe charakterizuje?", "dobrotivost", new List<string>()
         {
            "vzteklost",
            "hloupost",
            "dobrotivost"

         });
            _field[questionB.question_positionx, questionB.question_positiony].SetQuestion(questionB);
            _field[questionB.question_positionx, questionB.question_positiony].SetFieldType(FieldType.QUESTION);

            var questionC = new Question(2, 1, "Zabíjel jsi pavouky, které jsi našel v pokoji?", "ne", new List<string>()
         {
            "ano",
            "ne"

         });
            _field[questionC.question_positionx, questionC.question_positiony].SetQuestion(questionC);
            _field[questionC.question_positionx, questionC.question_positiony].SetFieldType(FieldType.QUESTION);

            var questionD = new Question(1, 2, "Co jsi měl za svého pozemského života nejraději?", "maminku", new List<string>()
         {
            "pivo",
            "maminku",
            "zabijeni"
         });
            _field[questionD.question_positionx, questionD.question_positiony].SetQuestion(questionD);
            _field[questionD.question_positionx, questionD.question_positiony].SetFieldType(FieldType.QUESTION);

            _field[0, 0].SetFieldType(FieldType.START);
            _field[2, 2].SetFieldType(FieldType.END);
            _field[0, 0].VisitField();
            Console.WriteLine("-----------------------");
            Console.WriteLine("FAMU UVÁDÍ");
            Console.WriteLine("-----------------------");
            Console.WriteLine("OČISTEC");
            Console.WriteLine("Tvé pozemské dny skončily. Zemřel jsi a nyní se rozhodne, kam bude putovat tvá duše. Projdi labyrintem spravedlnosti, odpověz na otázky, utkej se s vlastními démony a najdi cestu ven." +
               " Jakým jménem jsi se zval za svého pozemského života, nebožtíku?");

            player.Name = Console.ReadLine();

            _isRunnning = true;
            Console.WriteLine(player.Name + ", " + player.Name + "..." + " Někdo takový tu byl nedávno. Teď se vaří v pekelném kotli.");

            while (_isRunnning)
            {
                PlayerMovement(player);
                if (!_field[player.GetPosition().x, player.GetPosition().y].WasVisited())
                {
                    switch (_field[player.GetPosition().x, player.GetPosition().y].GetFieldType())
                    {
                        case FieldType.DEMON:
                            Demon d = _field[player.GetPosition().x, player.GetPosition().y].GetDemon();
                            Console.WriteLine("Potkal jsi démona, který představuje " + d.GetName());
                            Fight(player);
                            break;

                        case FieldType.QUESTION:
                            Question q = _field[player.GetPosition().x, player.GetPosition().y].GetQuestion();
                            Console.WriteLine("Odpověz pravdivě na následující otázku. " + q.GetQuestion());
                            q.ShowAnswers();
                            string yourAnswer = Console.ReadLine();
                            if (yourAnswer == q.CorrectAnswer)
                            {
                                Console.WriteLine("Jsi dobrý člověk!");
                                player.IncreaseKarma();
                            }
                            else
                            {
                                Console.WriteLine("Jsi ostuda lidstva.");
                                player.LoseKarma();
                            }
                            break;

                        case FieldType.END:
                            Console.WriteLine("Tvoje putování očistcem je u konce. Tvoje výsledná karma je " + player.GetKarma() + " a putuješ do ");
                            if (player.GetKarma() >= 3)
                            {
                                Console.Write("nebe. Hurá");
                            }
                            else
                            {
                                Console.Write("pekla. Měj se blaze.");
                            }
                            _isRunnning = false;
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Tady už jsi byl. Nic nového tu není.");
                }
                _field[player.GetPosition().x, player.GetPosition().y].VisitField();
            }

            Console.WriteLine("          " +
                "          " +
                "          ");

            Console.WriteLine("KONEC!");
            Console.WriteLine("---------------!");

            //Jak se zeptám, kde je hráč?
            // Console.WriteLine(  player.Name + " se nachází na pozici " + player.GetPosition().x + ", " + player.GetPosition().y);


        }
    }
}